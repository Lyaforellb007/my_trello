from fastapi import FastAPI
from api import auth
from api import workspace


app = FastAPI()

app.include_router(auth.router)
app.include_router(workspace.router)
