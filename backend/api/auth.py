from fastapi import (
    APIRouter,
    Depends,
    status,
)
from fastapi.security import OAuth2PasswordRequestForm

import models
from services.auth_services import Auth, get_current_user
from schemas import auth


router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)


@router.post(
    "/sign-up/",
    response_model=auth.Token,
    status_code=status.HTTP_201_CREATED,
)
def sign_up(
    user_data: auth.User,
    auth_service: Auth = Depends(),
):
    return auth_service.register_user(user_data)


@router.post(
    "/sign-in/",
    response_model=auth.Token,
)
def sign_in(
    auth_data: OAuth2PasswordRequestForm = Depends(),
    auth_service: Auth = Depends(),
):
    return auth_service.authenticate(
        auth_data.username,
        auth_data.password,
    )


@router.get(
    "/user/",
    response_model=auth.UserCreate,
)
def get_user(user: auth.UserCreate = Depends(get_current_user)):
    return user
