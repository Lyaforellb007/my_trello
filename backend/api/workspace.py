from fastapi import (
    APIRouter,
    Depends,
    status,
)
from services.auth_services import Auth, get_current_user
from services.workspace_service import WorkSpace

import models
from schemas import workspace
from schemas import auth

router = APIRouter(
    prefix="/workspace",
    tags=["workspace"],
)


@router.post(
    "/create/",
    response_model=workspace.WorkSpace,
    status_code=status.HTTP_201_CREATED,
)
def create(
    workspace_data: workspace.WorkSpace,
    user: auth.UserCreate = Depends(get_current_user),
    workspace_service: WorkSpace = Depends(),
):
    return workspace_service.create(workspace_data, user)
