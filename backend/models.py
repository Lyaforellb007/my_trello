from datetime import datetime

from sqlalchemy import Integer, String, ForeignKey, BOOLEAN, DateTime, Table, Sequence
from sqlalchemy.sql.schema import Column
from database import Base, engine
from sqlalchemy.orm import relationship
from sqlalchemy.orm import backref

Workspace_User_ID_SEQ = Sequence("Workspace_User_id_seq")


class Workspace_User(Base):
    __tablename__ = "workspace_user"

    id = Column(Integer, primary_key=True)
    user_email = Column(String(100), ForeignKey("user.email"))
    workspace_name = Column(String(100), ForeignKey("workspace.workspace_name"))


class User(Base):
    __tablename__ = "user"

    first_name = Column(String(100), nullable=False)
    surname = Column(String(100), nullable=False)
    email = Column(String(100), nullable=False, unique=True, primary_key=True)
    password = Column(String(100), nullable=False)
    workspace = relationship("Workspace_User", backref="user")


class Workspace(Base):
    __tablename__ = "workspace"

    workspace_name = Column(String(100), nullable=False, unique=True, primary_key=True)
    private = Column(BOOLEAN, nullable=False, default=True)
    image_url = Column(String)
    user = relationship("Workspace_User", backref="workspace")


#
# class Comment(Base):
#    __tablename__ = "comment"
#
#    id = Column(Integer, primary_key=True, nullable=False)
#    user_id = Column(Integer, ForeignKey("user.id"))
#    card_id = Column(Integer, ForeignKey("card.id"))
#    text = Column(String(100), nullable=False)
#    created_on = Column(DateTime(), default=datetime.now, nullable=False)
#    user = relationship("User")
#    card = relationship("Card")
#
#
# class List(Base):
#    __tablename__ = "list"
#
#    id = Column(Integer, primary_key=True)
#    workspace_id = Column(Integer, ForeignKey("workspace.id"))
#    name = Column(String(10), nullable=False, unique=True)
#    workspace = relationship("Workspace")
#    card = relationship("Card")
#
#
# class Card(Base):
#    __tablename__ = "card"
#
#    id = Column(Integer, primary_key=True)
#    list_id = Column(Integer, ForeignKey("list.id"))
#    card_name = Column(String(10), nullable=False, unique=True)
#    card_description = Column(String(50), nullable=False)
#    created_on = Column(DateTime(), default=datetime.now, nullable=False)
#    list = relationship("List")
#    comment = relationship("Comment")
#    card_user = relationship("Card_User")
#
#
# class Card_User(Base):
#    __tablename__ = "card_user"
#
#    id = Column(Integer, primary_key=True)
#    card_id = Column(Integer, ForeignKey("card.id"))
#    user_id = Column(Integer, ForeignKey("user.id"))
#    user = relationship("user.id")
#    card = relationship("card.id")'''
#
