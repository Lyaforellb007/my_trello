class Settings:
    server_host: str = "127.0.0.1"
    server_port: int = 8000
    jwt_secret = "str"
    jwt_algorithm = "HS256"
    jwt_expires_s = 3600
