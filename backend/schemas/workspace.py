from pydantic import BaseModel


class WorkSpace(BaseModel):
    workspace_name: str
    image_url: str
    private: bool
