from pydantic import BaseModel, EmailStr


class UserCreate(BaseModel):
    email: EmailStr
    first_name: str
    surname: str

    class Config:
        orm_mode = True


class User(UserCreate):
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str = "bearer"
