import jwt
from fastapi import (
    Depends,
    HTTPException,
    status,
)
from fastapi.security import OAuth2PasswordBearer
import jose
from jose import JWTError
from passlib.hash import bcrypt
from pydantic import ValidationError
from sqlalchemy.orm import Session

from database import get_db
import models
from schemas import auth
import settings

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/sign-in/")


def get_current_user(token: str = Depends(oauth2_scheme)):
    return Auth.verify_token(token)


class Auth:
    def hash_password(self, password):
        return bcrypt.hash(password)

    def verify_password(self, password, hash_password):
        return bcrypt.verify(password, hash_password)

    def create_token(self, user: models.User):
        user = auth.UserCreate.from_orm(user)
        payload = {
            "user": user.dict(),
        }
        token = jwt.encode(
            payload,
            settings.Settings().jwt_secret,
            algorithm=settings.Settings().jwt_algorithm,
        )
        return auth.Token(access_token=token)

    def verify_token(token: str) -> auth.UserCreate:
        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
        try:
            payload = jwt.decode(
                token,
                settings.Settings().jwt_secret,
                algorithms=[settings.Settings().jwt_algorithm],
            )
        except JWTError:
            raise exception from None

        user_data = payload.get("user")

        try:
            user = auth.UserCreate.parse_obj(user_data)
        except ValidationError:
            raise exception from None

        return user

    def __init__(self, session: Session = Depends(get_db)):
        self.session = session

    def register_user(self, user_data: auth.User) -> auth.Token:
        user = models.User(
            email=user_data.email,
            password=self.hash_password(user_data.password),
            first_name=user_data.first_name,
            surname=user_data.surname,
        )
        self.session.add(user)
        self.session.commit()
        return self.create_token(user)

    def authenticate(self, email, password):

        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
        user = (
            self.session.query(models.User).filter(models.User.email == email).first()
        )
        if not user:
            raise exception
        if not self.verify_password(password, user.password):
            raise exception
        return self.create_token(user)
