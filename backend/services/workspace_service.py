from database import get_db
import models
from schemas import auth
from sqlalchemy.orm import Session
from pydantic import ValidationError
from fastapi import (
    Depends,
    HTTPException,
    status,
)
from fastapi.security import OAuth2PasswordBearer
from schemas import workspace, auth


class WorkSpace:
    def __init__(self, session: Session = Depends(get_db)):
        self.session = session

    def create(self, workspace: workspace.WorkSpace, user: auth.UserCreate):
        WorkSpace = models.Workspace(
            workspace_name=workspace.workspace_name,
            private=workspace.private,
            image_url=workspace.image_url,
        )
        self.session.add(WorkSpace)
        self.session.commit()
        Workspace_User = models.Workspace_User(
            user_email=user.email, workspace_name=workspace.workspace_name
        )
        self.session.add(Workspace_User)
        self.session.commit()
        return workspace
